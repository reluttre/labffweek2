"""
@file main_week2.py
@brief main file that runs backend task

@author Robert Luttrell
@data 03/18/2021
"""
import pyb
import time
import encoder
import encoder_task
import backend_task

UPDATE_PERIOD_S = .005

backend_task1 = backend_task.BackendTask()

while True:
    backend_task1.run()
    time.sleep(UPDATE_PERIOD_S)
            
