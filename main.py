import pyb
import time
import encoder
import encoder_task
import backend_task

UPDATE_PERIOD_S = .005

"""
enc1 = encoder.Encoder(4, pyb.Pin.board.PB6, pyb.Pin.board.PB7)
enc2 = encoder.Encoder(3, pyb.Pin.board.PC6, pyb.Pin.board.PC7)
"""

backend_task1 = backend_task.BackendTask()

while True:
    backend_task1.run()
    time.sleep(UPDATE_PERIOD_S)
            
