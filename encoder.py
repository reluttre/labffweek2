INT16MAX = 65535
import pyb

class Encoder:

    def __init__(self, timer_num, pin1, pin2):
        self.timer = pyb.Timer(timer_num, prescaler=0, period=INT16MAX)
        self.ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin=pin1)
        self.ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin=pin2)
        self.position = 0
        self.old_pos_raw = 0
        self.pos_raw = 0

    def update(self):
        self.pos_raw = self.timer.counter()
        delta = self.pos_raw - self.old_pos_raw

        # Overflow
        if delta < -60000:
            delta += (INT16MAX + 1)

        # Underflow
        elif delta > 60000:
            delta -= (INT16MAX + 1)

        self.delta = delta
        self.position += delta

        self.old_pos_raw = self.pos_raw

    def get_position(self):
        return self.position

    def set_position(self, pos):
        self.position = pos

    def get_delta(self):
        return self.delta

