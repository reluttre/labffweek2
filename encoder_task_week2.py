"""
@file encoder_task_week2.py
@brief A progra for the encoder task

@author Robert Luttrell
@date 03/18/2021
"""
import encoder

class EncoderTask:

    def __init__(self, encoder_obj):
        self.enc = encoder_obj

    def run(self):
        self.enc.update()
        

