from pyb import UART
import utime
from math import *
import timeout_def

class Backend_Task:

    def __init__(self):
        self.TIMEOUT_S = timeout_def.TIMEOUT_S
        self.myuart = UART(2)
        self.state = -1

    def try_get_cmd(self):
        if self.myuart.any() != 0:
            val = self.myuart.readchar()
            return val
        else:
            return None

    def process_cmd(self, cmd):
        if cmd == ord("g"):
            return 0
        elif cmd == ord("s"):
            return 2
        else:
            return self.state

    def transmit_arr(self):
        for elem in self.data:
            self.myuart.write(str(elem).encode())

    def y_func(self, t):
        val = exp(-t/10)*sin(2*pi/3*t)
        return val

    def run(self):
        self.cmd = self.try_get_cmd()
        self.state = self.process_cmd(self.cmd)

        if self.state == -1:
            # wait for input
            pass

        if self.state == 0:
            # Start/reset counter
            self.data = []
            self.ticks_t0 = utime.ticks_ms()
            self.state = 1

        elif self.state == 1:
            # Add data to array
            ticks_t = utime.ticks_ms()
            t_ms = utime.ticks_diff(ticks_t, self.ticks_t0)
            t_s = t_ms / 1000
            y = self.y_func(t_s)
            out_string = '{:}, {:}\r\n'.format(t_s, y)
            self.data.append(out_string)
            if abs(t_s) > self.TIMEOUT_S:
                self.state = 2
            utime.sleep(0.13)

        elif self.state == 2:
            # Send data to computer
            self.transmit_arr()
            self.data = []
