"""
@file backend_task_week2.py
@brief A class acting as the backend task for the encoder data processing
@details Reads commands from the frontend, collects data from the encoders, and transmits that array to the frontend\n\n
Source code can be found at https://bitbucket.org/reluttre/labffweek2/src/master

@author Robert Luttrell
@data 03/18/2021
"""
from pyb import UART
import pyb
import utime
from math import *
import timeout_def
import encoder
import encoder_task

class BackendTask:

    def __init__(self):
        """
        @brief constructor for the task
        """
        self.TIMEOUT_S = timeout_def.TIMEOUT_S  # time before data collection stops
        self.myuart = UART(2)  # serial comm object
        self.state = -1
        self.enc1 = encoder.Encoder(4, pyb.Pin.board.PB6, pyb.Pin.board.PB7)  # encoder object
        self.enc_task = encoder_task.EncoderTask(self.enc1)  # encoder task

    def try_get_cmd(self):
        """
        @brief reads a command from serial object and returns one if in buffer, else returns None
        """
        if self.myuart.any() != 0:
            val = self.myuart.readchar()
            return val
        else:
            return None

    def process_cmd(self, cmd):
        """
        @brief returns the appropriate state given the command cmd
        @param cmd character representing command to be executed
        """
        if cmd == ord("g"):
            return 0
        elif cmd == ord("s"):
            return 5 
        elif cmd == ord("z"):
            return 2
        elif cmd == ord("p"):
            return 3
        elif cmd == ord("d"):
            return 4
        else:
            return self.state

    def transmit_arr(self):
        """
        @brief transmits data array over serial object
        """
        for elem in self.data:
            self.myuart.write(str(elem).encode())

    def run(self):
        """
        @brief main loop for task
        """
        self.cmd = self.try_get_cmd()
        self.state = self.process_cmd(self.cmd)

        if self.state == -1:
            # wait for input
            pass

        if self.state == 0:
            # Start/reset counter
            self.data = []
            self.ticks_t0 = utime.ticks_ms()
            self.state = 1

        elif self.state == 1:
            # Add data to array
            self.enc_task.run()
            ticks_t = utime.ticks_ms()
            t_ms = utime.ticks_diff(ticks_t, self.ticks_t0)
            t_s = t_ms / 1000
            enc_pos = self.enc1.get_position()
            out_string = '{:}, {:}\r\n'.format(t_s, enc_pos)
            self.data.append(out_string)
            if abs(t_s) > self.TIMEOUT_S:
                self.state = 2
            utime.sleep(0.13)

        elif self.state == 2:
            # Zero
            self.enc1.set_position(0)
            self.state = 1

        elif self.state == 3:
            # Print pos
            pos = self.enc1.get_position()
            self.myuart.write(str(pos).encode())

            self.state = 1

        elif self.state == 4:
            # Print delta
            delta = self.enc1.get_delta()
            self.myuart.write(str(delta).encode())
            self.state = 1

        elif self.state == 5:
            # Send data to computer
            self.transmit_arr()
            self.data = []
