"""
@file encoder_week2.py
@brief encoder class

@author Robert Luttrell
@date 03/18/2021
"""
INT16MAX = 65535
import pyb

class Encoder:

    def __init__(self, timer_num, pin1, pin2):
        """
        @brief Constructor method
        @param timer_num timer number to beused
        @param pin1 pin 1 of the encoder
        @param pin2 pin 2 of the encoder
        """
        self.timer = pyb.Timer(timer_num, prescaler=0, period=INT16MAX)
        self.ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin=pin1)
        self.ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin=pin2)
        self.position = 0
        self.old_pos_raw = 0
        self.pos_raw = 0
        self.delta = 0

    def update(self):
        """
        @brief updates the position of the encoder
        @details handles overflow by detecting unusually large delta values
        """
        self.pos_raw = self.timer.counter()
        delta = self.pos_raw - self.old_pos_raw

        # Overflow
        if delta < -60000:
            delta += (INT16MAX + 1)

        # Underflow
        elif delta > 60000:
            delta -= (INT16MAX + 1)

        self.delta = delta
        self.position += delta

        self.old_pos_raw = self.pos_raw

    def get_position(self):
        """
        @brief getter method for current position of encoder
        """
        return self.position

    def set_position(self, pos):
        """
        @brief setter method for current position of motor
        """
        self.position = pos

    def get_delta(self):
        """
        @brief getter method for delta value of motor
        @details delta represents the change between the previous and the current position
        """
        return self.delta

